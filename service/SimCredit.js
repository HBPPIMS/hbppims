var SimCredit;
(function (SimCredit) {
    var Calcul = (function () {
        function Calcul() {
            this.tableau = new TATableau();
        }
        Calcul.prototype.calculerMensualiteOLD = function (montantEmprunte, tauxPret, tauxAssurance, dureeEnMois) {
            var capitalAvantEcheance = montantEmprunte;
            var capitalApresEcheance = 0.0;
            var montantInterets = 0.0;
            var montantAssurance = 0.0;
            var mntCapitalRemb = 0.0;
            var periode = 0;
            var tauxGlobal = (tauxPret + tauxAssurance) / 100.0 / 12.0;
            var mensu = (montantEmprunte * tauxGlobal) / (1 - (1 / (Math.pow((1 + tauxGlobal), dureeEnMois))));
            mensu = Math.round(mensu * 100) / 100;
            //periode = 1;
            this.coutTotalCredit = 0;
            var annee = 2016;
            var mois = 1;
            for (var i = 1; i <= dureeEnMois; i++) {
                if (mois == 13) {
                    annee++;
                    mois = 1;
                }
                montantInterets = tauxPret / 12.0 / 100.0 * capitalAvantEcheance;
                montantAssurance = tauxAssurance / 12.0 / 100.0 * capitalAvantEcheance;
                mntCapitalRemb = mensu - montantInterets - montantAssurance;
                capitalApresEcheance = capitalAvantEcheance - mntCapitalRemb;
                this.tableau.AddAmortissement(annee, mois, capitalAvantEcheance, mensu, mntCapitalRemb, capitalApresEcheance, montantInterets, montantAssurance);
                capitalAvantEcheance = capitalApresEcheance;
                mois++;
                this.coutTotalCredit += montantInterets + montantAssurance;
            }
            return mensu;
        };
        Calcul.prototype.calculerMensualite = function (montantEmprunte, tauxPret, tauxAssurance, dureeEnMois) {
            var tauxGlobal = (tauxPret + tauxAssurance) / 100.0 / 12.0;
            var mensu = (montantEmprunte * tauxGlobal) / (1 - (1 / (Math.pow((1 + tauxGlobal), dureeEnMois))));
            mensu = Math.round(mensu * 100) / 100;
            return mensu;
        };
        Calcul.prototype.calculerDuree = function (montantEmprunte, tauxPret, tauxAssurance, mensualite) {
            var tauxGlobal = (tauxPret + tauxAssurance) / 100.0 / 12.0;
            var duree = (Math.log(mensualite) - Math.log(mensualite - tauxGlobal * montantEmprunte)) / (Math.log(1 + tauxGlobal));
            duree = Math.round(duree);
            return duree;
        };
        Calcul.prototype.calculerTAMensuel = function (montantEmprunte, tauxPret, tauxAssurance, mensualite, dureeEnMois) {
            var tableau = new TATableau;
            var capitalAvantEcheance = montantEmprunte;
            var capitalApresEcheance = 0.0;
            var montantInterets = 0.0;
            var montantAssurance = 0.0;
            var mntCapitalRemb = 0.0;
            var periode = 0;
            var tauxGlobal = (tauxPret + tauxAssurance) / 100.0 / 12.0;
            //mensu = Math.round(mensu * 100) / 100;
            //periode = 1;
            this.coutTotalCredit = 0;
            var annee = 2016;
            var mois = 1;
            for (var i = 1; i <= dureeEnMois; i++) {
                if (mois == 13) {
                    annee++;
                    mois = 1;
                }
                montantInterets = tauxPret / 12.0 / 100.0 * capitalAvantEcheance;
                montantAssurance = tauxAssurance / 12.0 / 100.0 * capitalAvantEcheance;
                mntCapitalRemb = mensualite - montantInterets - montantAssurance;
                capitalApresEcheance = capitalAvantEcheance - mntCapitalRemb;
                tableau.AddAmortissement(annee, mois, capitalAvantEcheance, mensualite, mntCapitalRemb, capitalApresEcheance, montantInterets, montantAssurance);
                capitalAvantEcheance = capitalApresEcheance;
                mois++;
                this.coutTotalCredit += montantInterets + montantAssurance;
            }
            return tableau;
        };
        Calcul.prototype.calculerTAAnnuel = function (montantEmprunte, tauxPret, tauxAssurance, mensualite, dureeEnMois) {
            var tableau = new TATableau;
            var tableauMensuel = this.calculerTAMensuel(montantEmprunte, tauxPret, tauxAssurance, mensualite, dureeEnMois);
            var annee = 0;
            var ligneCumul = new TALigne();
            for (var l in tableauMensuel.lignes) {
                var currentLine = tableauMensuel.lignes[l];
                /* Première itération, on enregistre la première année */
                if (annee == 0)
                    annee = currentLine.annee;
                /* Changement d'année */
                if (annee != currentLine.annee) {
                    /* On ajoute la ligne cumulée */
                    tableau.AddLigneAmortissement(ligneCumul);
                    /* On repart sur une nouvelle ligne */
                    ligneCumul = new TALigne();
                    annee = currentLine.annee;
                }
                ligneCumul.annee = currentLine.annee;
                ligneCumul.montantEcheance += currentLine.montantEcheance;
                ligneCumul.montantInterets += currentLine.montantInterets;
                ligneCumul.montantAssurance += currentLine.montantAssurance;
            }
            /* Dernière ligne */
            tableau.AddLigneAmortissement(ligneCumul);
            return tableau;
        };
        Calcul.prototype.Test = function () {
            return "Hello from Calculator!";
        };
        return Calcul;
    })();
    SimCredit.Calcul = Calcul;
    var TALigne = (function () {
        function TALigne() {
            this.montantEcheance = 0;
            this.montantInterets = 0;
            this.montantAssurance = 0;
        }
        return TALigne;
    })();
    var TATableau = (function () {
        function TATableau() {
            this.lignes = [];
        }
        TATableau.prototype.AddAmortissement = function (annee, mois, capitalReporte, montantEcheance, capitalRembourse, capitalRestantDu, montantInterets, montantAssurance) {
            var ligne = new TALigne();
            ligne.periode = "" + annee + pad(mois, 2);
            ligne.annee = annee;
            ligne.mois = mois;
            ligne.capitalReporte = capitalReporte;
            ligne.montantEcheance = montantEcheance;
            ligne.capitalRembourse = capitalRembourse;
            ligne.capitalRestantDu = capitalRestantDu;
            ligne.montantInterets = montantInterets;
            ligne.montantAssurance = montantAssurance;
            this.lignes.push(ligne);
        };
        TATableau.prototype.AddLigneAmortissement = function (ligne) {
            this.lignes.push(ligne);
        };
        return TATableau;
    })();
    function pad(n, width, z) {
        if (z === void 0) { z = "0"; }
        z = z || '0';
        n = n + '';
        return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
    }
})(SimCredit || (SimCredit = {}));
function CalculerDureeFromMensualite(capital, tauxCredit, tauxAss, mensualite) {
    var calc = new SimCredit.Calcul();
    var duree = calc.calculerDuree(capital, tauxCredit, tauxAss, mensualite);
    return duree;
}
function CalculerMensualiteFromDuree(capital, tauxCredit, tauxAss, duree) {
    var calc = new SimCredit.Calcul();
    var mensualite = calc.calculerMensualite(capital, tauxCredit, tauxAss, duree);
    return mensualite;
}
function TableauAmortissement(capital, tauxCredit, tauxAss, dureeEnMois, mensualite) {
    var calc = new SimCredit.Calcul();
    var tab = calc.calculerTAAnnuel(capital, tauxCredit, tauxAss, mensualite, dureeEnMois);
    /* Transformation du tableau pour le rendre compatible */
    var resultTab = new Array();
    var lignetmp = ['Date', 'Mensualités'];
    resultTab.push(lignetmp);
    //var i = 0;
    for (var t in tab.lignes) {
        var l = tab.lignes[t];
        var ligne = [l.annee.toString(), l.montantEcheance];
        resultTab.push(ligne);
    }
    return resultTab;
}
