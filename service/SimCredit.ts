﻿module SimCredit {
    export class Calcul {
        coutTotalCredit: number;
        tableau: TATableau;


        constructor() {
            this.tableau = new TATableau();
        }

        calculerMensualiteOLD(montantEmprunte: number, tauxPret: number, tauxAssurance: number, dureeEnMois: number): number {
            var capitalAvantEcheance = montantEmprunte;
            var capitalApresEcheance = 0.0;
            var montantInterets = 0.0;
            var montantAssurance = 0.0;
            var mntCapitalRemb = 0.0;
            var periode = 0;
            var tauxGlobal = (tauxPret + tauxAssurance) / 100.0 / 12.0;

            var mensu = (montantEmprunte * tauxGlobal) / (1 - (1 / (Math.pow((1 + tauxGlobal), dureeEnMois))));

            mensu = Math.round(mensu * 100) / 100;

            //periode = 1;
            this.coutTotalCredit = 0;
            var annee = 2016;
            var mois = 1;
            for (var i = 1; i <= dureeEnMois; i++) {
                if (mois == 13) {
                    annee++;
                    mois = 1;
                }
                montantInterets = tauxPret / 12.0 / 100.0 * capitalAvantEcheance;
                montantAssurance = tauxAssurance / 12.0 / 100.0 * capitalAvantEcheance;
                mntCapitalRemb = mensu - montantInterets - montantAssurance;
                capitalApresEcheance = capitalAvantEcheance - mntCapitalRemb;
                this.tableau.AddAmortissement(annee, mois, capitalAvantEcheance, mensu, mntCapitalRemb, capitalApresEcheance, montantInterets, montantAssurance);
                capitalAvantEcheance = capitalApresEcheance;
                mois++;
                this.coutTotalCredit += montantInterets + montantAssurance;
            }

            return mensu;
        }

        calculerMensualite(montantEmprunte: number, tauxPret: number, tauxAssurance: number, dureeEnMois: number): number {

            var tauxGlobal = (tauxPret + tauxAssurance) / 100.0 / 12.0;

            var mensu = (montantEmprunte * tauxGlobal) / (1 - (1 / (Math.pow((1 + tauxGlobal), dureeEnMois))));

            mensu = Math.round(mensu * 100) / 100;

            return mensu;
        }

        calculerDuree(montantEmprunte: number, tauxPret: number, tauxAssurance: number, mensualite: number): number {

            var tauxGlobal = (tauxPret + tauxAssurance) / 100.0 / 12.0;
            var duree = (Math.log(mensualite) - Math.log(mensualite - tauxGlobal * montantEmprunte)) / (Math.log(1 + tauxGlobal));

            duree = Math.round(duree);

            return duree;
        }

        calculerTAMensuel(montantEmprunte: number, tauxPret: number, tauxAssurance: number, mensualite: number, dureeEnMois: number): TATableau {
            var tableau = new TATableau;
            var capitalAvantEcheance = montantEmprunte;
            var capitalApresEcheance = 0.0;
            var montantInterets = 0.0;
            var montantAssurance = 0.0;
            var mntCapitalRemb = 0.0;
            var periode = 0;
            var tauxGlobal = (tauxPret + tauxAssurance) / 100.0 / 12.0;

            //mensu = Math.round(mensu * 100) / 100;

            //periode = 1;
            this.coutTotalCredit = 0;
            var annee = 2016;
            var mois = 1;
            for (var i = 1; i <= dureeEnMois; i++) {
                if (mois == 13) {
                    annee++;
                    mois = 1;
                }
                montantInterets = tauxPret / 12.0 / 100.0 * capitalAvantEcheance;
                montantAssurance = tauxAssurance / 12.0 / 100.0 * capitalAvantEcheance;
                mntCapitalRemb = mensualite - montantInterets - montantAssurance;
                capitalApresEcheance = capitalAvantEcheance - mntCapitalRemb;
                tableau.AddAmortissement(annee, mois, capitalAvantEcheance, mensualite, mntCapitalRemb, capitalApresEcheance, montantInterets, montantAssurance);
                capitalAvantEcheance = capitalApresEcheance;
                mois++;
                this.coutTotalCredit += montantInterets + montantAssurance;
            }

            return tableau;
        }

        calculerTAAnnuel(montantEmprunte: number, tauxPret: number, tauxAssurance: number, mensualite: number, dureeEnMois: number): TATableau {
            var tableau = new TATableau;

            var tableauMensuel = this.calculerTAMensuel(montantEmprunte, tauxPret, tauxAssurance, mensualite, dureeEnMois);

            var annee = 0;
            var ligneCumul = new TALigne();
            for (var l in tableauMensuel.lignes) {
                var currentLine = tableauMensuel.lignes[l];

                /* Première itération, on enregistre la première année */
                if (annee == 0) annee = currentLine.annee;
                
                /* Changement d'année */
                if (annee != currentLine.annee) {
                    /* On ajoute la ligne cumulée */
                    tableau.AddLigneAmortissement(ligneCumul);

                    /* On repart sur une nouvelle ligne */
                    ligneCumul = new TALigne();
                    annee = currentLine.annee;
                }

                ligneCumul.annee = currentLine.annee;
                ligneCumul.montantEcheance += currentLine.montantEcheance;
                ligneCumul.montantInterets += currentLine.montantInterets;
                ligneCumul.montantAssurance += currentLine.montantAssurance;
            }

            /* Dernière ligne */
            tableau.AddLigneAmortissement(ligneCumul);

            return tableau;
        }



        Test() {
            return "Hello from Calculator!";
        }
    }


    class TALigne {
        periode: string;
        annee: number;
        mois: number;
        capitalReporte: number;
        montantEcheance: number;
        capitalRembourse: number;
        montantInterets: number;
        montantAssurance: number;
        capitalRestantDu: number;

        constructor() {
            this.montantEcheance = 0;
            this.montantInterets = 0;
            this.montantAssurance = 0;
        }
    }

    class TATableau {
        lignes: TALigne[];

        constructor() {
            this.lignes = [];
        }

        AddAmortissement(
            annee: number,
            mois: number,
            capitalReporte: number,
            montantEcheance: number,
            capitalRembourse: number,
            capitalRestantDu: number,
            montantInterets: number,
            montantAssurance: number) {

            var ligne = new TALigne();

            ligne.periode = "" + annee + pad(mois, 2);
            ligne.annee = annee;
            ligne.mois = mois;
            ligne.capitalReporte = capitalReporte;
            ligne.montantEcheance = montantEcheance;
            ligne.capitalRembourse = capitalRembourse;
            ligne.capitalRestantDu = capitalRestantDu;
            ligne.montantInterets = montantInterets;
            ligne.montantAssurance = montantAssurance;

            this.lignes.push(ligne);

        }

        AddLigneAmortissement(ligne: TALigne) {
            this.lignes.push(ligne);
        }


    }

    function pad(n, width, z = "0") {
        z = z || '0';
        n = n + '';
        return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
    }


    
    
    /*
    var calc = new Calcul();

    document.body.innerHTML = "Mensualité : " + calc.calculerMensualite(100000, 1.5, 0.0, 180);
    document.body.innerHTML += "<br/>Durée : " + 
    document.body.innerHTML += "</br><hr>";
    //document.body.innerHTML += JSON.stringify(calc.calculerTAMensuel(100000, 1.5, 0.0, 180, 620));

    document.body.innerHTML += JSON.stringify(calc.calculerTAAnnuel(100000, 1.5, 0.0, 180, 620));*/
}

function CalculerDureeFromMensualite(capital: number, tauxCredit: number, tauxAss: number, mensualite: number) {
    var calc = new SimCredit.Calcul();

    var duree = calc.calculerDuree(capital, tauxCredit, tauxAss, mensualite);

    return duree;
}

function CalculerMensualiteFromDuree(capital: number, tauxCredit: number, tauxAss: number, duree: number) {
    var calc = new SimCredit.Calcul();

    var mensualite = calc.calculerMensualite(capital, tauxCredit, tauxAss, duree);

    return mensualite;
}

function TableauAmortissement(capital: number, tauxCredit: number, tauxAss: number, dureeEnMois : number, mensualite: number) {
    var calc = new SimCredit.Calcul();

    var tab = calc.calculerTAAnnuel(capital, tauxCredit, tauxAss, mensualite, dureeEnMois);

    /* Transformation du tableau pour le rendre compatible */
    var resultTab = new Array();

    var lignetmp = ['Date', 'Mensualités'];

    resultTab.push(lignetmp);
    //var i = 0;
    for (var t in tab.lignes) {
        var l = tab.lignes[t];
        var ligne = [l.annee.toString(), l.montantEcheance];
        resultTab.push(ligne);
    }

    return resultTab;
}