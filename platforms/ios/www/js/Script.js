﻿
$(function () {

    jQuery.support.cors = true;


    $("body").on("click", ".maison", function () {
		ModifBloc1("fa-home");
    });
    $("body").on("click", ".appartement", function () {
		ModifBloc1("fa-th-large");
    });
    $("body").on("click", ".immeuble", function () {
		ModifBloc1("fa-building");
    });
	
    $("body").on("click", ".returnTo1", function () {
		$(".bloc1").attr("class", "fa fa-5x bloc1 fa-plus-square-o");
		$(".Ecran2").fadeOut(300);
		$(".Ecran1").delay(400).fadeIn(300);
    });
    $("body").on("click", ".returnTo2", function () {
		$(".bloc2").attr("class", "fa fa-5x bloc2 fa-plus-square-o");
		$(".Ecran3").fadeOut(300);
		$(".Ecran2").delay(400).fadeIn(300);
    });
    $("body").on("click", ".returnTo3", function () {
		$(".bloc3").attr("class", "fa fa-5x bloc3 fa-plus-square-o");
		$(".Ecran4").fadeOut(300);
		$(".Ecran3").delay(400).fadeIn(300);
    });
    $("body").on("click", ".returnTo4", function () {
		$(".bloc3").attr("class", "fa fa-5x bloc4 fa-plus-square-o");
		$(".Ecran4").fadeOut(300);
		$(".Ecran3").delay(400).fadeIn(300);
    });

    $("body").on("click", ".addEmprunteur1", function () {
        $(".Emprunteur1A").css("display", "none");
        $(".Emprunteur1B").css("display", "block");
    });
    $("body").on("click", ".addEmprunteur2", function () {
        $(".Emprunteur2A").css("display", "none");
        $(".Emprunteur2B").css("display", "block");
    });
    $("body").on("click", ".addEmprunteur3", function () {
        $(".Emprunteur3A").css("display", "none");
        $(".Emprunteur3B").css("display", "block");
    });

    $("body").on("click", ".boutonSex1", function () {
        $(".boutonSex1").removeClass("active");
        $(this).addClass("active");
    });
    $("body").on("click", ".boutonSex2", function () {
        $(".boutonSex2").removeClass("active");
        $(this).addClass("active");
    });
    $("body").on("click", ".boutonSex3", function () {
        $(".boutonSex3").removeClass("active");
        $(this).addClass("active");
    });

	function ModifBloc1(NewClass)
	{
	    $(".bloc1").attr("class", "fa fa-5x bloc1 " + NewClass);
	    $(".textbloc1").html("Type");
		$(".Ecran1").fadeOut(300);
		$(".Ecran2").delay(400).fadeIn(300);
	}
	
    $("body").on("click", ".neuf", function () {
        ModifBloc2("fa-check-square-o");
    });
    $("body").on("click", ".ancien", function () {
		ModifBloc2("fa-check-square-o");
    });
    $("body").on("click", ".validerEtape3", function () {
		ModifBloc3("fa-male");
    });
    $("body").on("click", ".validerEtape4", function () {
        ModifBloc4("fa-eur");
    });

    $("body").on("click", ".finaliser", function () {
        Finaliser();
    });
    $("body").on("click", ".retoursimu", function () {
        mail();
        RetourSimu();
    });

    function mail() {
        window.location.href = "mailto:gilles.leclerc@ouest.banquepopulaire.fr,pascal.trimouillat@massifcentral.banquepopulaire.fr,patrick.bianchetti@bpce.fr,frederic.champion@i-bp.fr,jean-noel.lemaitre@i-bp.fr?subject=Votre simulation de crédit immobilier&body=<body>Bonjour Monsieur Hacka Tom,<br/><br/>J’ai été heureux de vous accueillir ce jour en agence pour évoquer votre projet de crédit immobilier. Vous trouverez ci-dessous mes coordonnées, je reste à votre entière disposition pour approfondir votre besoin.<br/><br/>Cordialement,<br/><br/>Sophie<br/><br/>Agence Bordeaux Saint Jean<br/>05 71 57 75 33<br/>Sophie@bpaca.fr</body>";
    }
	
	function ModifBloc2(NewClass)
    {
	    //$(".bloc2a").css("display", "none");
	    //$(".bloc2b").css("display", "block");
	    //$(".bloc2b").attr("class", "ancienb");
	    $(".bloc2").attr("class", "fa fa-5x bloc2 " + NewClass);
	    $(".textbloc2").html("Etat");

		$(".Ecran2").fadeOut(300);
		$(".Ecran3").delay(400).fadeIn(300);
	}

	function ModifBloc3(NewClass)
    {
	    $(".bloc3").attr("class", "fa fa-5x bloc3 " + NewClass);
	    $(".textbloc3").html("Emprunteur");
	    $(".Ecran3").fadeOut(300);
		$(".Ecran4").delay(400).fadeIn(300);
	}
	function ModifBloc4(NewClass)
    {
	    $(".bloc4").attr("class", "fa fa-5x bloc4 " + NewClass);
	    $(".textbloc4").html("Besoin");
	    $(".Ecran4").fadeOut(300);
		$(".panel-footer").fadeOut(300);
		$(".Ecran5").delay(400).fadeIn(300);
		$(".panel-body").height(1000);

		$("#montant").val($("#montantPret").val());

		var slider2 = $("#SMontant").data("ionRangeSlider");
		slider2.update({
		    min: 0,
		    max: 300000,
		    from: $("#montant").val()
		});

		$("#duree").val($("#duree4").val());
		var slider1 = $("#SDuree").data("ionRangeSlider");
		slider1.update({
		    min: 0,
		    max: 240,
		    from: $("#duree").val()
		});


	}
	
	function Finaliser() {
	    $(".Ecran5").fadeOut(300);
	    $(".Ecran6").delay(400).fadeIn(300);
	}

	function RetourSimu() {
	    $(".Ecran6").fadeOut(300);
	    $(".Ecran5").delay(400).fadeIn(300);
	}


		// function ModifBloc2(NewClass)
	// {
		// $(".bloc2").attr("class", NewClass);
		// $(".Ecran2").fadeOut(300);
		// $(".Ecran3").delay(400).fadeIn(300);
	// }

	$('#ionRangeSlider1').ionRangeSlider({
	    min: 18,
	    max: 80,
	    step: 0,
	    from: 18
	});
	$('#ionRangeSlider2').ionRangeSlider({
	    min: 18,
	    max: 80,
	    step: 0,
	    from: 18
	});
	$('#ionRangeSlider3').ionRangeSlider({
	    min: 18,
	    max: 80,
	    step: 0,
	    from: 18
	});


});