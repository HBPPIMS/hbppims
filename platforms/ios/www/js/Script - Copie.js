﻿if (!('forEach' in Array.prototype)) {
    Array.prototype.forEach = function (action, that /*opt*/) {
        for (var i = 0, n = this.length; i < n; i++)
            if (i in this)
                action.call(that, this[i], i, this);
    };
}

/////////////////////////////////////////////////
// Detection de la version d'Internet Explorer //
/////////////////////////////////////////////////
var ie = (function () {

    var undef,
        v = 3,
        div = document.createElement('div'),
        all = div.getElementsByTagName('i');

    while (
        div.innerHTML = '<!--[if gt IE ' + (++v) + ']><i></i><![endif]-->',
        all[0]
    );

    return v > 4 ? v : undef;

}());

///////////////////////////////////////////////////////////////
// Fonction de delay utilisé pour la recherche (keyup delay) //
///////////////////////////////////////////////////////////////
var delay = (function () {
    var timer = 0;
    return function (callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
    };
})();

$(function () {

    jQuery.support.cors = true;

    ////////////////////////////////////////////
    // Bouton dropdown active si enfant actif //
    ////////////////////////////////////////////
    $("nav .nav .dropdown .dopdown-menu > li.active").parents(".dropdown").addClass("active");

    //////////////////////////////////////////////////////////////////
    // Largeur de l'input "#collab" = largeur du bouton de dropdown //
    //////////////////////////////////////////////////////////////////
    $("#collab").outerWidth($("#collabDropdown").outerWidth());

    //////////////////////////
    // tableau rayé sur IE8 //
    //////////////////////////
    if (ie < 9 && $(".table-striped tbody tr").length != 0) {
        $(".table-striped tbody").each(function () {
            $("tr:even", this).css("background-color", "#EFF3F4");
        })
    }

    ////////////////////////////////
    // Affiche d'un message flash //
    ///////////////////////////////
    function AjouterFlash(Type, Content) {

        var id = Math.floor((Math.random() * 1000));

        $("#flashbox").prepend("<div id='flash_" + id + "' class='alert alert-dismissible fade in " + Type + "' role='alert'><button type='button' class='close' data-dismiss='alert'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button>" + Content + "</div>");

        setTimeout(function () {
            $("#flash_" + id).alert('close');
        }, 5000)
    }
    ////////////////////////////////////////////
    // Affiche d'un message flash dans une div//
    ////////////////////////////////////////////
    function AjouterFlashPerso(Type, Content, div) {

        var id = Math.floor((Math.random() * 1000));

        div.html("<div id='flash_" + id + "' class='alert alert-dismissible fade in " + Type + "' role='alert'><button type='button' class='close' data-dismiss='alert'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button>" + Content + "</div>");

        setTimeout(function () {
            $("#flash_" + id).alert('close');
        }, 3000)
    }

    /////////////////////////////////////////////
    // Supprime flashes après un certian temps //
    /////////////////////////////////////////////
    setTimeout(function () {
        $(".flash").alert('close');
    }, 5000)

    //////////////////////////////////////////////////
    // Appel AJAX pour récuperer les collab pour SU //
    //////////////////////////////////////////////////
    $("#collab").on("keyup", function () {
        var that = this;
        $("#collabProgress").show();
        delay(function () {
            if ($(that).val() == "") {
                $("#collab").parent().nextAll().remove();
                $("#collabProgress").hide();
            }
            else {
                var url = $("#collab").data("url") + "/" + $(that).val();
                $.ajax({
                    type: "GET",
                    url: url,
                    success: function (result) {
                        $("#collabProgress").hide();
                        $("#collab").parent().nextAll().remove();
                        for (var i = 0; i < result.length; i++) {
                            var suUrl = window.location.href + "?su_mode=" + result[i].matricule;
                            $(that).parent().after("<li><a href=\"" + suUrl + "\"><p>" + result[i].nom + "</p><p>" + result[i].matricule + "</p></a></li>");
                        }
                    }
                });
            }
        }, 300);
    });

    ////////////////////////////////////////////
    // Tablesorter : Filters, sorting, paging //
    ////////////////////////////////////////////
    $(".ts-pager").each(function (index) {
        $(this).addClass("ts-pager" + index);
    });
    $(".pagenum").each(function (index) {
        $(this).addClass("pagenum" + index);
    });
    $(".reset").each(function (index) {
        $(this).addClass("reset" + index);
    });

    $.extend($.tablesorter.themes.bootstrap, {
        // these classes are added to the table. To see other table classes available,
        // look here: http://twitter.github.com/bootstrap/base-css.html#tables
        table: 'table table-bordered table-striped',
        caption: 'caption',
        header: 'bootstrap-header', // give the header a gradient background
        footerRow: '',
        footerCells: '',
        icons: '', // add "icon-white" to make them white; this icon class is added to the <i> in the header
        sortNone: 'bootstrap-icon-unsorted',
        sortAsc: 'glyphicon glyphicon-chevron-up',     // includes classes for Bootstrap v2 & v3
        sortDesc: 'glyphicon glyphicon-chevron-down', // includes classes for Bootstrap v2 & v3
        active: '', // applied when column is sorted
        hover: '', // use custom css here - bootstrap class may not override it
        filterRow: '', // filter row class
        even: '', // odd row zebra striping
        odd: ''  // even row zebra striping
    });

    // call the tablesorter plugin and apply the uitheme widget
    $(".tablesorter").each(function (index) {
        $(this).tablesorter({
            // this will apply the bootstrap theme if "uitheme" widget is included
            // the widgetOptions.uitheme is no longer required to be set
            theme: "bootstrap",

            dateFormat: "ddmmyyyy", // set the default date format

            // Enable use of the characterEquivalents reference
            sortLocaleCompare: true,

            headerTemplate: '{content} {icon}', // new in v2.7. Needed to add the bootstrap icon!

            // widget code contained in the jquery.tablesorter.widgets.js file
            // use the zebra stripe widget if you plan on hiding any rows (filter widget)
            widgets: ["uitheme", "filter", "zebra", "resizable"],

            widgetOptions: {
                // using the default zebra striping class name, so it actually isn't included in the theme variable above
                // this is ONLY needed for bootstrap theming if you are using the filter widget, because rows are hidden
                zebra: ["even", "odd"],

                // reset filters button
                filter_reset: ".reset" + index,

                // extra css class name (string or array) added to the filter element (input or select)
                filter_cssFilter: "form-control",

                // set the uitheme widget to use the bootstrap theme class names
                // this is no longer required, if theme is set
                // ,uitheme : "bootstrap"

                // resizable options
                resizable: true
            }
        })
        .tablesorterPager({

            // target the pager markup - see the HTML block below
            container: ".ts-pager" + index,

            // target the pager page select dropdown - choose a page
            cssGoto: ".pagenum" + index,

            // remove rows from the table to speed up the sort of large tables.
            // setting this to false, only hides the non-visible rows; needed if you plan to add/remove rows with the pager enabled.
            removeRows: false,

            // output string - default is '{page}/{totalPages}';
            // possible variables: {page}, {totalPages}, {filteredPages}, {startRow}, {endRow}, {filteredRows} and {totalRows}
            output: '{startRow} - {endRow} / {filteredRows} ({totalRows})'

        });
    });

    ///////////////////////////////////////
    // Dropdown avec input a l'interieur //
    ///////////////////////////////////////
    $(document).on('click', '.dropdown-menu', function (e) {
        $(this).hasClass('keep_open') && e.stopPropagation(); // This replace if conditional.
    });

    ////////////////
    // Datepicker //
    ////////////////
    $('[data-rel=datepicker]').datepicker({
        autoclose: true,
        todayHighlight: true,
        language: "fr"
    });


    //////////////////////////////////////////////////////////////////////////
    // Permet de la spinner si la validation des formulaires est incorrecte //
    //////////////////////////////////////////////////////////////////////////
    $("form").on("invalid-form", function () {
        RemoveSpinnerAttente();
    });


    /////////////////////////////////////////////////////////////
    // Permet d'ajouter un spinner sur un bouton après le clic //
    /////////////////////////////////////////////////////////////
    $("body").on("click", ".attente", function () {
        document.getElementsByTagName('body')[0].style.cursor = 'wait';

        $(this).attr("disabled", "disabled");
        $(this).append(" <i class=\"fa fa-spinner fa-spin spinner-attente\"></i>");
    });
    // Remove le spinner du bouton
    function RemoveSpinnerAttente() {
        $(".spinner-attente").parents("button").removeAttr('disabled');
        $(".spinner-attente").parents("a").removeAttr('disabled');
        $(".spinner-attente").remove();

        document.getElementsByTagName('body')[0].style.cursor = 'default';

    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Initialisation des composants utilisés dans les pages (peut être appellé dans le complete des AJAX) //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    InitComposants();
    function InitComposants() {
        $(".bootstrap-switch").bootstrapSwitch("destroy");
        $(".bootstrap-switch").bootstrapSwitch();
        $(".bootstrap-switch-id-cbAllerRetour").css("display", "block")

        $('.tagsinput').tagsinput({
            tagClass: function (item) {
                return 'label label-info';
            }
        });

        // Active select2 sur les select/option
        $(".form-select2").select2();

        $("#ddlVehicule").select2({
            "language": {
                "noResults": function () {
                    return "Vous n'avez aucun véhicule, vous devez accéder au menu \"Profil\" pour ajouter un véhicule";
                }
            },
        });

        $(".select-add-icon").select2({
            templateResult: AddIconToDDL,
            templateSelection: AddIconToDDL
        });


        // For performance reasons, the Tooltip and Popover data-apis are opt-in, meaning you must initialize them yourself.
        $('[data-toggle="popover"]').popover();
        $('[data-toggle="tooltip"]').tooltip();


        AddRules();
    }


    ////////////////////////////////////////////////////////////////////////
    // Fonction permettant d'ajouter des règles de saisie à certain champ //
    ////////////////////////////////////////////////////////////////////////
    function AddRules() {
        if ($(".kilometrage").length != 0) {
            $(".kilometrage").rules("add", {
                pattern: /^(\d+|\d+,\d{1,3})$/,
                messages: {
                    pattern: "Veuillez fournir un kilométrage valide."
                }
            });
        }
        if ($(".montant").length != 0) {
            $(".montant").rules("add", {
                pattern: /^(\d+|\d+,\d{1,2})$/,
                messages: {
                    pattern: "Veuillez fournir un montant valide."
                }
            });
        }
        if ($(".decimal").length != 0) {
            $(".decimal").rules("add", {
                pattern: /^(\d+|\d+,\d+)$/,
                messages: {
                    pattern: "Veuillez fournir une valeur décimale."
                }
            });
        }
        if ($(".entier").length != 0) {
            $(".entier").rules("add", {
                pattern: /^(\d+|\d+)$/,
                messages: {
                    pattern: "Veuillez fournir un nombre entier valide."
                }
            });
        }
    }

    ////////////////////////////////////////////////////////
    // Fonction permettant d'ajouter un icon dans une DDL //
    ////////////////////////////////////////////////////////
    function AddIconToDDL(state) {
        if (!state.id) { return state.text; }

        var icon = state.text.split("|")[0];
        var text = state.text.split("|")[1];
        var $state = $(
          "<span><i class='fa " + icon + "'></i> " + text + "</span>"
        );
        return $state;
    };

    //////////////////////////////////////////////////////////////////
    // Permet de remplacer le "." par une "," dans un champ decimal //
    //////////////////////////////////////////////////////////////////
    $("body").on("keyup", ".decimal", function () {
        var valeur = $(this).val();
        valeur = valeur.replace(".", ",");
        $(this).val(valeur);
    });

    ///////////////////////////////////////////////////
    // Appels AJAX pour la recherche d'une depense //
    ///////////////////////////////////////////////////
    $("body").on("submit", "#formRechercherDemande", function (e) {

        var IdDemande = $("#txtIdDemandeRecherchee").val();

        if (isNaN(parseInt(IdDemande))) {
            AjouterFlashPerso("alert-danger", "N° incorrect", $("#flashboxPerso"));
            RemoveSpinnerAttente();
            return false;
        }

        var url = $(this).data("url");

        $.ajax({
            type: "POST",
            url: url,
            data: {
                Id: IdDemande
            },
            success: function (result) {
                if (result.Etat == "ko") {
                    AjouterFlashPerso("alert-danger", result.Message, $("#flashboxPerso"));
                    RemoveSpinnerAttente();
                }
                else {
                    window.document.location = result.Url;
                }
            },
            error: function (error) {
                alert(error.responseText);
                RemoveSpinnerAttente();
            },
            complete: function () {
            }
        });

        e.preventDefault();
    });

    //////////////////////////////////////////////////////////////////////////////////
    // Appels AJAX pour récuperer la page de sélection d'un PDO sous forme de modal //
    //////////////////////////////////////////////////////////////////////////////////
    $("#divPDO .btn").click(function () {
        $("#modal .modal-dialog").width($(this).data("width"));

        var base = "<div class=\"modal-header\">";
        base += "<button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>";
        base += "</div>";
        base += "<div class=\"modal-body text-center\" style=\"font-size: 10em;\">";
        base += "<i class=\"fa fa-fw fa-spinner fa-spin\"></i>";
        base += "</div>";
        $(".modal-content").html(base);

        var url = $(this).data("url");
        $.ajax({
            type: "POST",
            url: url,
            success: function (result) {
                $(".modal-content").html(result);
            },
            error: function () {
                alert("Error");
            },
            complete: function () {
                // Le champ select n'est pas dans le DOM car affiché en ALAX
                InitComposants()
            }
        });
    });

    // Onclick sur le bouton de validation de la sélection d'un PDO depuis la modal
    // On part du "body" car le bouton n'est pas dans le DOM car affiché en ALAX
    $("body").on("click", "#btnConfirmSelectPDO", function () {
        $("#txtIdPDO").val($("#ddlPDO option:selected").val());
        $("#txtPDO").val($("#ddlPDO option:selected").text());
    });

    ///////////////////////////////////////////////////////////////////////////
    // Appels AJAX pour afficher la liste des champs de saisie d'une dépense //
    ///////////////////////////////////////////////////////////////////////////
    $("body").on("click", ".ItemNatureFrais", function () {
        $(".ItemNatureFrais").removeClass("active");
        $(this).addClass("active");

        var url = $(this).data("url");
        var idNatureFrais = $(this).data("id");

        AfficherListChampsDeSaisieDepense(url, idNatureFrais);
    });
    $("body").on("click", ".ItemTypeDeFrais", function () {
        $(".ItemTypeDeFrais").removeClass("active");
        $(this).addClass("active");

        var url = $(this).data("url");
        var idNatureFrais = 0;

        AfficherListChampsDeSaisieDepense(url, idNatureFrais);

        if ($(this).data("pdo") == "1") {
            $("#divPDO").hide();
        }
        else {
            $("#divPDO").show();
        }
    });

    function AfficherListChampsDeSaisieDepense(url, idNatureFrais) {
        document.getElementsByTagName('body')[0].style.cursor = 'wait';

        var strMatricule = $("#MatriculeNoteDeFrais").val();
        var iAnnee = $("#AnneeNoteDeFrais").val();
        var iMois = $("#MoisNoteDeFrais").val();

        var iIdTypeFrais = $(".ItemTypeDeFrais.active:first").data("id");
        var iIdNatureFrais = idNatureFrais;

        var strObjetDeplacement = $("#txtObjet").val();

        var strAnimateur = $("#txtAnimateur").val();

        $.ajax({
            type: "POST",
            url: url,
            data: {
                strMatricule: strMatricule,
                iAnneeNDF: iAnnee,
                iMoisNDF: iMois,
                idTypeFrais: iIdTypeFrais,
                idNatureFrais: iIdNatureFrais,
                strObjetDeplacement: strObjetDeplacement,
                strAnimateur: strAnimateur
            },
            success: function (result) {
                $("#divChampsAjoutFrais").html(result);

                AddRules();
            },
            error: function (error) {
                alert(error.responseText);
            },
            complete: function () {
                // Les champs ne sont pas dans le DOM car affichés en ALAX
                InitComposants();

                document.getElementsByTagName('body')[0].style.cursor = 'default';
            }
        });
    }

    ///////////////////////////////////////////////////
    // Permet de calculer le nb de km entre 2 villes //
    ///////////////////////////////////////////////////
    $("body").on("click", "#btnCalculerKM", function () {
        showLocation($("#txtDepart").val(), $("#txtArrivee").val(), true);
    });
    $("body").on("switchChange.bootstrapSwitch", ".cbAllerRetourKM", function () {
        showLocation($("#txtDepart").val(), $("#txtArrivee").val(), false);
    });
    $("body").on("change", ".calculerKM", function () {
        showLocation($("#txtDepart").val(), $("#txtArrivee").val(), false);
    });
    var geocoder, location1, location2, gDir, interv;
    function initialize() {
        geocoder = new GClientGeocoder();
        gDir = new GDirections();

        GEvent.addListener(gDir, "load", function () {
            var drivingDistanceKilometers = gDir.getDistance().meters / 1000;

            var strDepart = location1.ville;
            if (location1.country != "France")
                strDepart += ", " + location1.country;
            $("#txtDepart").val(strDepart);

            var strArrivee = location2.ville;
            if (location2.country != "France")
                strArrivee += ", " + location2.country;
            $("#txtArrivee").val(strArrivee);

            if ($('#cbAllerRetour').prop("checked"))
                drivingDistanceKilometers = drivingDistanceKilometers * 2;
            $('#txtNbKm').val(parseInt(drivingDistanceKilometers));

            CalculerMontantFraisKilometrique();

            clearInterval(interv);
            document.getElementsByTagName('body')[0].style.cursor = 'default';
        });
    }
    function incremente() {
        alert("Désolés, nous ne parvenons pas à calculer la distance");
        clearInterval(interv);
        document.getElementsByTagName('body')[0].style.cursor = 'default';
    }
    function showLocation(depart, arrivee, AvecAlert) {
        $('#txtNbKm').val("");
        $('#txtMontant').val("");

        initialize();
        try {
            interv = setInterval("incremente()", 5000);
        }
        catch (e) {

        }
        document.getElementsByTagName('body')[0].style.cursor = 'wait';

        geocoder.getLocations(depart, function (response) {

            if (!response || response.Status.code != 200) {
                if (AvecAlert)
                    alert("Désolés, nous ne parvenons pas à localiser l'adresse de départ");
                clearInterval(interv);
                document.getElementsByTagName('body')[0].style.cursor = 'default';
            }
            else {
                try {
                    location1 = { country: response.Placemark[0].AddressDetails.Country.CountryName, ville: response.Placemark[0].AddressDetails.Country.AdministrativeArea.Locality.LocalityName };
                    if (location1.country != "France") {
                        if (AvecAlert)
                            alert("Désolés, nous ne parvenons pas à localiser l'adresse de départ");
                        clearInterval(interv);
                        document.getElementsByTagName('body')[0].style.cursor = 'default';
                    }
                }
                catch (e) {
                    if (AvecAlert)
                        alert("Désolés, nous ne parvenons pas à localiser l'adresse de départ");
                    clearInterval(interv);
                    document.getElementsByTagName('body')[0].style.cursor = 'default';

                }
                geocoder.getLocations(arrivee, function (response) {
                    if (!response || response.Status.code != 200) {
                        if (AvecAlert)
                            alert("Désolés, nous ne parvenons pas à localiser l'adresse d'arrivée");
                        clearInterval(interv);
                        document.getElementsByTagName('body')[0].style.cursor = 'default';
                    }
                    else {
                        try {
                            location2 = { country: response.Placemark[0].AddressDetails.Country.CountryName, ville: response.Placemark[0].AddressDetails.Country.AdministrativeArea.Locality.LocalityName };
                            if (location2.country != "France") {
                                if (AvecAlert)
                                    alert("Désolés, nous ne parvenons pas à localiser l'adresse d'arrivée");
                                clearInterval(interv);
                                document.getElementsByTagName('body')[0].style.cursor = 'default';
                            }
                            else
                                gDir.load('from: ' + location1.ville + ' to: ' + location2.ville);
                        }
                        catch (e) {
                            if (AvecAlert)
                                alert("Désolés, nous ne parvenons pas à localiser l'adresse d'arrivée");
                            clearInterval(interv);
                            document.getElementsByTagName('body')[0].style.cursor = 'default';

                        }
                    }
                });
            }
        });
    }

    /////////////////////////////////////////////////////////////////
    // Permet de calculer le montant de remboursement kilométrique //
    /////////////////////////////////////////////////////////////////
    $("body").on("change", "#ddlVehicule", function () {
        CalculerMontantFraisKilometrique();
    });
    $("body").on("keyup", "#txtNbKm", function () {
        CalculerMontantFraisKilometrique();
    });
    function CalculerMontantFraisKilometrique() {

        var dMontantParKilometre = 0;
        try {
            dMontantParKilometre = parseFloat($("#ddlVehicule").val().split("|")[1].replace(",", "."));
        }
        catch (e) {
            alert("Vous devez créer un véhicule avant de pouvoir ajouter des frais kilométriques !")
        }

        var iNbKm = parseInt($('#txtNbKm').val());
        if (isNaN(iNbKm))
            iNbKm = 0;

        var dMontant = (dMontantParKilometre * iNbKm).toFixed(2);

        $('#txtMontant').val(dMontant.replace(".", ","));
    }

    ///////////////////////////////////////////
    // Permet de calculer le prix d'une nuit //
    ///////////////////////////////////////////
    $("body").on("keyup", ".calcul-prix-nuit", function () {

        try {
            var PrixParNuit = parseFloat($('#txtMontant').val().replace(",", ".")) / $('#txtNbNuits').val();
            PrixParNuit = Math.floor(PrixParNuit * 100) / 100;

            if (!isNaN(PrixParNuit) && isFinite(PrixParNuit))
                $('#txtPrixParNuit').val(PrixParNuit.toString().replace(".", ","));
        }
        catch (e) {
            $('#txtPrixParNuit').val("");
        }
    });

    //////////////////////////////////////////
    // Appels AJAX pour ajouter une dépense //
    //////////////////////////////////////////
    $("body").on("submit", "#formAjoutDepense", function (e) {

        var url = $(this).data("url");

        var Matricule = $("#MatriculeNoteDeFrais").val();
        var iAnnee = $("#AnneeNoteDeFrais").val();
        var iMois = $("#MoisNoteDeFrais").val();

        var dtDate = $("#txtDateFrais").val();

        var iIdTypeFrais = $(".ItemTypeDeFrais.active:first").data("id");

        var strObjet = $("#txtObjet").val();
        var strAnimateur = $("#txtAnimateur").val();

        var iIdNatureFrais = $(".ItemNatureFrais.active:first").data("id");
        if (iIdNatureFrais == null || iIdNatureFrais == "")
            iIdNatureFrais = 0;

        var strPDO = $("#txtIdPDO").val();

        var iIdVehicule = $("#ddlVehicule").val();
        if (iIdVehicule == null || iIdVehicule == "")
            iIdVehicule = 0;
        else
            iIdVehicule = iIdVehicule.split("|")[0];

        var strDepart = $("#txtDepart").val();
        var strArrivee = $("#txtArrivee").val();

        var boolAllerRetour = false;
        if ($('#cbAllerRetour').prop("checked"))
            boolAllerRetour = true;

        var fNbKm = $('#txtNbKm').val();
        if (fNbKm == null || fNbKm == "")
            fNbKm = 0;

        var dMontant = $('#txtMontant').val();
        if (dMontant == null || dMontant == "")
            dMontant = 0;

        var strDetail = $("#txtDetail").val();

        var iNbNuits = $("#txtNbNuits").val();
        if (iNbNuits == null || iNbNuits == "")
            iNbNuits = 0;

        var strCorrespondances = $("#txtCorrespondances").val();

        var boolIsDansCirconscription = false;
        var boolIsAParis = false;
        if ($("#ddlRegion").val() != null) {
            if ($("#ddlRegion").val().split("|")[0] == 1) {
                boolIsDansCirconscription = true;
            }
            if ($("#ddlRegion").val().split("|")[1] == 1) {
                boolIsAParis = true;
            }
        }

        var iNbInvites = $("#txtNbInvites").val();
        if (iNbInvites == null || iNbInvites == "")
            iNbInvites = 0;

        var strNomInvites = $("#txtNomInvites").val();

        var iIdTypeRepas = $("#ddlTypeRepas").val();
        if (iIdTypeRepas == null || iIdTypeRepas == "")
            iIdTypeRepas = 0;

        $.ajax({
            type: "POST",
            url: url,
            data: {
                strMatricule: Matricule,
                iAnneeNDF: iAnnee,
                iMoisNDF: iMois,
                dtDate: dtDate,
                idTypeFrais: iIdTypeFrais,
                strPDO: strPDO,
                strObjet: strObjet,
                strAnimateur: strAnimateur,
                idNatureFrais: iIdNatureFrais,
                iIdVehicule: iIdVehicule,
                strDepart: strDepart,
                strArrivee: strArrivee,
                boolAllerRetour: boolAllerRetour,
                fNbKm: fNbKm,
                dMontant: dMontant,
                strDetail: strDetail,
                strCorrespondances: strCorrespondances,
                iNbNuits: iNbNuits,
                boolIsDansCirconscription: boolIsDansCirconscription,
                boolIsAParis: boolIsAParis,
                iNbInvites: iNbInvites,
                strNomInvites: strNomInvites,
                iIdTypeRepas: iIdTypeRepas
            },
            success: function (result) {
                if (result.Etat != null) {
                    if (result.Etat == "ko") {
                        AjouterFlash("alert-danger", "Erreur lors de l'ajout de la dépense");
                    }
                }
                else {
                    // Recherche de l'Id de la demande
                    var iIndex1 = result.indexOf("Demande n°") + 10;
                    var iIndex2 = result.indexOf(" ", iIndex1);
                    var IdDemande = result.substr(iIndex1, iIndex2 - iIndex1);

                    // Si la demande est déja affichée, on la recharge
                    if (document.getElementById("divDemande_" + IdDemande) != null)
                        $("#divDemande_" + IdDemande).html(result);
                    else {// Si la demande n'est pas affichée, on ajout une nouvelle div
                        if ($("#divLstDemandes div:first").length == 0)
                            $("#divLstDemandes").append("<div id='divDemande_" + IdDemande + "' class='col-xs-12 divDemande' style='display:none'>" + result + "</div>");
                        else
                            $("#divLstDemandes div:first").before("<div id='divDemande_" + IdDemande + "' class='col-xs-12 divDemande' style='display:none'>" + result + "</div>");

                        $("#divDemande_" + IdDemande).slideDown(200);
                    }
                }
            },
            error: function (error) {
                alert(error.responseText);
            },
            complete: function () {
                InitComposants();

                RemoveSpinnerAttente();
            }
        });

        e.preventDefault();
    });

    ///////////////////////////////////////////////////
    // Appels AJAX pour la suppression d'une depense //
    ///////////////////////////////////////////////////
    $("body").on("click", ".btnSuppDepense", function () {
        var divDemande = $(this).parents(".divDemande");

        var url = $(this).data("url");

        $.ajax({
            type: "POST",
            url: url,
            success: function (result) {

                if (result.Etat != null) {
                    if (result.Etat == "DemandeSupprimee") {
                        divDemande.fadeOut();
                        AjouterFlash("alert-success", "Dépense et demande supprimées avec succès");
                    }
                    if (result.Etat == "ko") {
                        AjouterFlash("alert-danger", "Erreur lors de la suppression de la dépense");
                    }
                }
                else {
                    divDemande.html(result);
                }
            },
            error: function (error) {
                alert(error.responseText);
            },
            complete: function () {
                InitComposants();

                RemoveSpinnerAttente();
            }
        });
    });

    ///////////////////////////////////////////////////
    // Appels AJAX pour la suppression d'une demande //
    ///////////////////////////////////////////////////
    $("body").on("click", ".btnSuppDemande", function () {

        if (!confirm("Etes-vous sûr de vouloir supprimer cette demande ?")) {
            RemoveSpinnerAttente();
            return;
        }
        var divDemande = $(this).parents(".divDemande");
        divDemande.fadeOut();

        var url = $(this).data("url");

        $.ajax({
            type: "POST",
            url: url,
            success: function (result) {

                if (result.Etat != "ok") {
                    divDemande.fadeIn();
                    AjouterFlash("alert-danger", "Erreur lors de la suppression de la demande");
                }
                else
                    AjouterFlash("alert-success", "Demande supprimée avec succès");
            },
            error: function (error) {
                alert(error.responseText);
            },
            complete: function () {
                RemoveSpinnerAttente();
            }
        });
    });

    ////////////////////////////////////////////////////////////
    // Appels AJAX pour l'envoi pour validation d'une demande //
    ////////////////////////////////////////////////////////////
    $("body").on("click", ".btnEnvoiValidationDemande", function () {

        if (!confirm("Etes-vous sûr de vouloir envoyer cette demande pour validation ?")) {
            RemoveSpinnerAttente();
            return;
        }

        var divDemande = $(this).parents(".divDemande");

        var url = $(this).data("url");

        $.ajax({
            type: "POST",
            url: url,
            success: function (result) {

                if (result.Etat != null) {
                    if (result.Etat == "ko") {
                        AjouterFlash("alert-danger", "Erreur lors de l'envoi pour validation de la demande");
                    }
                }
                else {
                    divDemande.html(result);
                }
            },
            error: function (error) {
                alert(error.responseText);
            },
            complete: function () {
                InitComposants();

                RemoveSpinnerAttente();
            }
        });
    });

    /////////////////////////////////////////////////////
    // Appels AJAX pour la réception des justificatifs //
    /////////////////////////////////////////////////////
    $("body").on("click", ".btnReceptionnerJustifs", function () {

        var divDemande = $(this).parents(".divDemande");

        var url = $(this).data("url");

        $.ajax({
            type: "POST",
            url: url,
            success: function (result) {

                if (result.Etat != null) {
                    if (result.Etat == "ko") {
                        AjouterFlash("alert-danger", "Erreur lors de la réception des justificatifs de la demande");
                    }
                }
                else {
                    divDemande.html(result);
                }
            },
            error: function (error) {
                alert(error.responseText);
            },
            complete: function () {
                InitComposants();

                RemoveSpinnerAttente();
            }
        });
    });

    //////////////////////////////////////////////////
    // Appels AJAX pour la validation d'une demande //
    //////////////////////////////////////////////////
    $("body").on("click", ".btnValiderDemande", function () {

        if (!confirm("Etes-vous sûr de vouloir valider cette demande ?")) {
            RemoveSpinnerAttente();
            return;
        }

        var divDemande = $(this).parents(".divDemande");

        var url = $(this).data("url");

        $.ajax({
            type: "POST",
            url: url,
            success: function (result) {

                if (result.Etat != null) {
                    if (result.Etat == "ko") {
                        AjouterFlash("alert-danger", "Erreur lors de la validation de la demande");
                    }
                    else if (result.Etat == "Redirect") {
                        window.location = result.Url;
                    }
                }
                else {
                    divDemande.html(result);
                }
            },
            error: function (error) {
                alert(error.responseText);
            },
            complete: function () {
                InitComposants();

                RemoveSpinnerAttente();
            }
        });
    });

    $("body").on("change", ".cbControlJustif", function () {

        if ($(this).prop("checked")) {
            $("#btnValiderDemande_" + $(this).data("iddemande")).prop("disabled", false);
        }
        else {
            $("#btnValiderDemande_" + $(this).data("iddemande")).prop("disabled", true);
        }
    });

    //////////////////////////////////////////////////
    // Appels AJAX pour le traitement d'une demande //
    //////////////////////////////////////////////////
    $("body").on("submit", ".formDemande", function (e) {

        if (!confirm("Etes-vous sûr de vouloir valider la configuration de cette demande ?")) {
            RemoveSpinnerAttente();
            return;
        }

        var divDemande = $(this).parents(".divDemande");

        var url = $(this).data("url");

        var ddlCodeOperation = $(".ddlCodeOperation");

        var lstCode = [];
        var i = 0;
        ddlCodeOperation.each(
                                function () {
                                    var array = [$(this).data("iddepense"), $(this).val()];
                                    lstCode[i] = array;
                                    i++;
                                }
                            );

        $.ajax({
            type: "POST",
            url: url,
            data: {
                lstCode: JSON.stringify(lstCode)
            },
            success: function (result) {

                if (result.Etat != null) {
                    if (result.Etat == "ko") {
                        AjouterFlash("alert-danger", "Erreur lors de la validation dela configuration de la demande");
                    }
                }
                else {
                    divDemande.html(result);
                }
            },
            error: function (error) {
                alert(error.responseText);
            },
            complete: function () {
                InitComposants();

                RemoveSpinnerAttente();
            }
        });

        e.preventDefault();

    });

    //////////////////////////////////////////////////////
    // Appels AJAX pour retourner au collab une demande //
    //////////////////////////////////////////////////////
    $("body").on("click", ".btnRetournerDemande", function () {

        $("#modal .modal-dialog").width($(this).data("width"));

        var base = "<div class=\"modal-header\">";
        base += "<button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>";
        base += "<h4 class=\"modal-title\">Saisissez le motif du retour</h4>";
        base += "</div>";

        base += "<div class=\"modal-body\">";
        base += "<form id=\"formRetourCollab\" data-url=\"" + $(this).data("url") + "\" data-iddemande=\"" + $(this).data("iddemande") + "\">";
        base += "<div class=\"row\">";
        base += "	<div class=\"col-xs-12\">";
        base += "		<div class=\"form-group\">";
        base += "			<label class=\"control-label\" for=\"lbMotifRetour\">Motif</label>";
        base += "			<textarea rows=\"5\" name=\"txtMotifRetour\" class=\"form-control\" id=\"txtMotifRetour\" aria-required=\"true\" required=\"required\" ></textarea>";
        base += "		</div>";
        base += "	</div>";
        base += "	<div class=\"col-xs-12 text-center\">";
        base += "		<button id=\"btnConfirmModifMontant\" class=\"btn btn-primary attente\" type=\"submit\">Ok</button>";
        base += "		<button class=\"btn btn-default\" type=\"button\" data-dismiss=\"modal\">Annuler</button>";
        base += "	</div>";
        base += "</div>";
        base += "</form>";
        base += "</div>";

        $(".modal-content").html(base);

        $("#formRetourCollab").validate({
            invalidHandler: function (event, validator) {
                RemoveSpinnerAttente();
            }
        });
    });

    // Onclick sur le bouton de validation du motif de retour depuis la modal
    $("body").on("submit", "#formRetourCollab", function (e) {
        var divDemande = $("#divDemande_" + $(this).data("iddemande"));
        var url = $(this).data("url");

        var strMotifRetour = $('#txtMotifRetour').val();

        $.ajax({
            type: "POST",
            url: url,
            data: {
                strMotifRetour: strMotifRetour
            },
            success: function (result) {
                if (result.Etat != null) {
                    if (result.Etat == "ko") {
                        AjouterFlash("alert-danger", "Erreur lors du retour de la demande");
                    }
                }
                else {
                    divDemande.html(result);
                }
            },
            error: function (error) {
                alert(error.responseText);
            },
            complete: function () {
                // Fermeture de la modal
                $('#modal').modal('hide');

                InitComposants();

                RemoveSpinnerAttente();

            }
        });

        e.preventDefault();

    });

    ////////////////////////////////////////////////////////////////////////////
    // Methodes pour la modification du montant d'une dépense par le valideur //
    ////////////////////////////////////////////////////////////////////////////
    $("body").on("click", ".btnModifMontantDepense", function () {

        $("#modal .modal-dialog").width($(this).data("width"));

        var base = "<div class=\"modal-header\">";
        base += "<button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>";
        base += "<h4 class=\"modal-title\">Saisissez le nouveau montant</h4>";
        base += "</div>";

        base += "<div class=\"modal-body\">";
        base += "<form id=\"formModifMontant\" data-url=\"" + $(this).data("url") + "\" data-iddemande=\"" + $(this).data("iddemande") + "\">";
        base += "<div class=\"row\">";
        base += "	<div class=\"col-xs-6\">";
        base += "		<div class=\"form-group\">";
        base += "			<label class=\"control-label\" for=\"lbNewMontant\">Nouveau montant</label>";
        base += "			<input name=\"txtNewMontant\" class=\"form-control decimal montant\" id=\"txtNewMontant\" aria-required=\"true\" required=\"required\" type=\"text\" value=\"" + $(this).data("montant") + "\">                    ";
        base += "		</div>";
        base += "	</div>";
        base += "	<div class=\"col-xs-12\">";
        base += "		<div class=\"form-group\">";
        base += "			<label class=\"control-label\" for=\"lbCommentaire\">Commentaire</label>";
        base += "			<textarea rows=\"5\" name=\"txtCommentaire\" class=\"form-control\" id=\"txtCommentaire\" ></textarea>";
        base += "		</div>";
        base += "	</div>";
        base += "	<div class=\"col-xs-12 text-center\">";
        base += "		<button id=\"btnConfirmModifMontant\" class=\"btn btn-primary attente\" type=\"submit\">Ok</button>";
        base += "		<button class=\"btn btn-default\" type=\"button\" data-dismiss=\"modal\">Annuler</button>";
        base += "	</div>";
        base += "</div>";
        base += "</form>";
        base += "</div>";

        $(".modal-content").html(base);

        $("#formModifMontant").validate({
            invalidHandler: function (event, validator) {
                RemoveSpinnerAttente();
            }
        });

        AddRules();
    });

    // Onclick sur le bouton de validation de la saisie du nouveau montant depuis la modal
    $("body").on("submit", "#formModifMontant", function (e) {
        var divDemande = $("#divDemande_" + $(this).data("iddemande"));
        var url = $(this).data("url");

        var dNewMontant = $('#txtNewMontant').val();
        if (dNewMontant == null || dNewMontant == "")
            dNewMontant = 0;

        var strCommentaireModif = $('#txtCommentaire').val();

        $.ajax({
            type: "POST",
            url: url,
            data: {
                dNewMontant: dNewMontant,
                strCommentaireModif: strCommentaireModif
            },
            success: function (result) {
                if (result.Etat != null) {
                    if (result.Etat == "ko") {
                        AjouterFlash("alert-danger", "Erreur lors de la modification du montant de la dépense");
                    }
                }
                else {
                    divDemande.html(result);
                }
            },
            error: function (error) {
                alert(error.responseText);
            },
            complete: function () {
                // Fermeture de la modal
                $('#modal').modal('hide');

                InitComposants();

                RemoveSpinnerAttente();
            }
        });

        e.preventDefault();

    });

    /////////////////////////////////////////////
    // Methodes pour l'impression du bordereau //
    /////////////////////////////////////////////
    $("body").on("click", ".btnImprimerBodereau", function (e) {

        //window.open($(this).data("url"), "_blank");
        window.open($(this).data("url"), "Impression bordereau", "menubar=no, status=no, menubar=no, width=640, height=800");

        RemoveSpinnerAttente();

    });

    ///////////////////////////////////////////////
    // Methodes pour la validation d'un véhicule //
    ///////////////////////////////////////////////
    $("body").on("click", ".btnOuvrirValiderVehicule", function () {

        $("#modal .modal-dialog").width($(this).data("width"));

        var base = "<div class=\"modal-header\">";
        base += "<button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>";
        base += "</div>";
        base += "<div class=\"modal-body text-center\" style=\"font-size: 10em;\">";
        base += "<i class=\"fa fa-fw fa-spinner fa-spin\"></i>";
        base += "</div>";
        $(".modal-content").html(base);

        var url = $(this).data("url");
        $.ajax({
            type: "POST",
            url: url,
            success: function (result) {
                $(".modal-content").html(result);
            },
            error: function () {
                alert("Error");
            },
            complete: function () {
                InitComposants();

                $(":file").filestyle({ buttonText: "Parcourir" });
            }
        });
    });

    $("body").on("change", ".cbControlCarteGrise", function () {

        if ($(this).prop("checked")) {
            $("#btnValiderVehicule").prop("disabled", false);
        }
        else {
            $("#btnValiderVehicule").prop("disabled", true);
        }
    });

    // Onclick sur le bouton de validation du véhicule
    $("body").on("click", "#btnValiderVehicule", function (e) {

        var url = $(this).data("url");
        $.ajax({
            type: "POST",
            url: url,
            data: {
            },
            success: function (result) {
                if (result.Etat != null) {
                    if (result.Etat == "ok")
                        window.location.reload();
                    else
                        AjouterFlash("alert-danger", "Erreur lors de la validation du véhicule");
                }
            },
            error: function (error) {
                alert(error.responseText);
            },
            complete: function () {
            }
        });
    });


    /////////////////////////////////////////////////////////////////////////////////
    // Methodes pour appliquer la sélection du code opération aux lignes suivantes //
    /////////////////////////////////////////////////////////////////////////////////
    $("body").on("click", ".btnAppliquerLignesSuivantes", function () {
        var valACopier = $("#ddlCodeOperation_" + $(this).data("iddepense")).val();

        var ddlCodeOperation = $(".ddlCodeOperation");

        var lstDDL = $(this).parents("tr").nextAll("tr").find(ddlCodeOperation);

        if (lstDDL.length != 0)
            lstDDL.each(function () { $(this).select2("val", valACopier) });
    });

    /////////////////////////////////////////////////////////////////////////
    // Méthodes pour l'affichage du détail d'une NDF dans la liste des NDF //
    /////////////////////////////////////////////////////////////////////////
    function formatDetailNDF(data) {

        if (data == null)
            return "";

        var lstDemande = data[4].split("|");

        var retour = "";

        var i;
        if (lstDemande != "") {
            for (i = 0 ; i < lstDemande.length ; i++) {


                var lstDonnees = lstDemande[i].split(";");

                retour += "<div class='col-xs-offset-0 col-xs-12 list-group-item'>";

                // Badge et Id demande
                retour += "<h5 class='list-group-item-heading bold'>Demande n°" + lstDonnees[1] + " (" + lstDonnees[7] + " €)";
                // Badge
                retour += " <span class='badge badge-" + lstDonnees[0] + "'>";
                // Etat
                retour += lstDonnees[2] + "</span>";
                retour += "</h5>";

                retour += "<p class='list-group-item-text'>";
                // Date d'envoi pour validation
                if (lstDonnees[3] != "") {
                    retour += "Envoyée pour validation le " + lstDonnees[3].substring(0, 10);
                }
                // Date de validation
                if (lstDonnees[4] != "") {
                    retour += " - Validée le " + lstDonnees[4].substring(0, 10);
                    // Valideur
                    if (lstDonnees[5] != "") {
                        retour += " par " + lstDonnees[5];
                    }
                }
                // Date de paiement
                if (lstDonnees[6] != "") {
                    retour += " - Payée le " + lstDonnees[6].substring(0, 10);
                }

                retour += "</p>"
                retour += "</div>"

            }
        }


        return retour;

    }
    var $rowDetailsTable = $('#tabNDF');
    // Insert a 'dt-details-control' column to the table
    $rowDetailsTable.find('thead tr, tfoot tr').each(function () {
        this.insertBefore(document.createElement('th'), this.childNodes[0]);
    });
    $rowDetailsTable.find('tbody tr').each(function () {
        $(this).prepend('<td class="dt-details-control"><i class="fa fa-fw dt-details-toggle"></i></td>');
    });
    var rowDetailsTable = $rowDetailsTable.dataTable({
        'processing': true,
        'aoColumnDefs': [
            { 'bSortable': true, 'aTargets': [0] }
        ],
        //'order': [
        //    [3, 'desc'],
        //    [1, 'desc']
        //],
        searching: false,
        paging: false,
        ordering: false,
        'info': false,
        "language": {
            'emptyTable': 'Aucune note de frais pour le moment.'
        }
    });
    $rowDetailsTable.find('tbody').on('click', 'tr td:first-child', function () {
        var tr = $(this).parents('tr');
        var row = $rowDetailsTable.api().row(tr);
        if (row.child.isShown()) {
            tr.removeClass('details');
            row.child.hide();

        } else {
            tr.addClass('details');
            row.child(formatDetailNDF(row.data())).show();
        }
    });

    ///////////////////////////////////////////////////
    // Méthodes pour la création d'une note de frais // 
    ///////////////////////////////////////////////////
    $("#btnAddNoteDeFrais").click(function () {

        $("#modal .modal-dialog").width($(this).data("width"));

        var base = "<div class=\"modal-header\">";
        base += "<button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>";
        base += "</div>";
        base += "<div class=\"modal-body text-center\" style=\"font-size: 10em;\">";
        base += "<i class=\"fa fa-fw fa-spinner fa-spin\"></i>";
        base += "</div>";
        $(".modal-content").html(base);

        var url = $(this).attr("data-path");
        $.ajax({
            type: "GET",
            url: url,
            cache: false,
            success: function (result) {
                $(".modal-content").html(result);
            },
            error: function (error) {
                alert(error.responseText);
            },
            complete: function () {
                $('#dpSelectMois').datepicker({
                    startDate: "01/04/2015",
                    startView: 1,
                    minViewMode: 1,
                    language: "fr"
                });
                $('#dpSelectMois').on("changeDate", function () {
                    $('#txtSelectMois').val(
                        $('#dpSelectMois').datepicker('getFormattedDate')
                    );
                });
                $(".datepicker-inline").css("display", "inline-block");
            }
        });
        ///////////////////////////////////////////////////
    });
    $("body").on("click", "#btnOkSelectMois", function () {
        var DateSelectionnee = $("#txtSelectMois").val();

        if (DateSelectionnee == "") {
            alert("Veuillez sélectionner le mois correspondant aux frais à rembourser !")
            RemoveSpinnerAttente();
            return;
        }

        var Mois = DateSelectionnee.substring(3, 5);
        var Annee = DateSelectionnee.substring(6, 10);

        var url = $(this).data("url");

        url += "&Annee=" + Annee + "&Mois=" + Mois;

        window.document.location = url;
    });

    /////////////////////////////////////////////////////////////////////////////
    // Affichage de la modal pour la modification ou la création d'un véhicule //
    /////////////////////////////////////////////////////////////////////////////
    $(".btnDetailVehicule").click(function () {

        $("#modal .modal-dialog").width($(this).data("width"));

        var base = "<div class=\"modal-header\">";
        base += "<button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>";
        base += "</div>";
        base += "<div class=\"modal-body text-center\" style=\"font-size: 10em;\">";
        base += "<i class=\"fa fa-fw fa-spinner fa-spin\"></i>";
        base += "</div>";
        $(".modal-content").html(base);

        var url = $(this).attr("data-path");
        $.ajax({
            type: "GET",
            url: url,
            cache: false,
            success: function (result) {
                $(".modal-content").html(result);
            },
            error: function (error) {
                alert(error.responseText);
            },
            complete: function () {
                $("#formDetailVehicule").validate({
                    errorPlacement: function (error, element) {
                        if (element.hasClass("form-select2")) {
                            error.appendTo(element.next(".select2"));
                        }
                        else if (element.hasClass("icheck")) {
                            error.insertAfter(element.parents(".form-group").children(".control-label")[0]);
                        }
                        else {
                            error.insertAfter(element);
                        }
                    }
                });

                InitComposants();

                $(":file").filestyle({ buttonText: "Parcourir" });
            }
        });
    });

    ////////////////////////////////////////////////////////////////////////////////
    // Contôle de la taille du ficher de carte grise lors du submit du formulaire //
    ////////////////////////////////////////////////////////////////////////////////
    $("body").on("submit", "#formDetailVehicule", function (e) {

        if (document.getElementById("FichierCarteGrise").files[0] != null) {
            var FileSize = document.getElementById("FichierCarteGrise").files[0].size;
            if (FileSize > 5000000) // > 5Mo
            {
                alert("Enregistrement impossible, le fichier ne doit pas dépasser 5 Mo !");
                e.preventDefault();
                return;
            }
        }
    });

    ///////////////////////////////////////////////////
    // Appels AJAX pour la suppression d'un véhicule //
    ///////////////////////////////////////////////////
    $("body").on("click", ".btnDelVehicule", function () {

        if (!confirm("Etes-vous sûr de vouloir supprimer ce véhicule ?")) {
            RemoveSpinnerAttente();
            return;
        }

        var url = $(this).data("url");

        window.document.location = url;
    });

    ///////////////////////////////////////////////////
    // Appels AJAX pour la suppression d'un véhicule //
    ///////////////////////////////////////////////////
    $("body").on("change", ".rbVehiculeParDefaut", function () {

        var url = $(this).data("url");

        $.ajax({
            type: "POST",
            url: url,
            success: function (result) {

                if (result.Etat != null) {
                    if (result.Etat == "ok")
                        AjouterFlash("alert-success", "Modification du véhicule par défaut effectuée");
                    else
                        AjouterFlash("alert-danger", "Erreur lors de la modification du véhicule par défaut");
                }
            },
            error: function (error) {
                alert(error.responseText);
            },
            complete: function () {
            }
        });
    });


    //////////////////////////////////////////////////////////////
    // Methodes pour la modificatioon d'un compte collaborateur //
    //////////////////////////////////////////////////////////////
    $("body").on("click", "#btnOuvrirUpdateCompte", function () {

        $("#modal .modal-dialog").width($(this).data("width"));

        var base = "<div class=\"modal-header\">";
        base += "<button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>";
        base += "</div>";
        base += "<div class=\"modal-body text-center\" style=\"font-size: 10em;\">";
        base += "<i class=\"fa fa-fw fa-spinner fa-spin\"></i>";
        base += "</div>";
        $(".modal-content").html(base);

        var url = $(this).data("url");
        $.ajax({
            type: "POST",
            url: url,
            success: function (result) {
                $(".modal-content").html(result);
            },
            error: function (error) {
                alert(error.responseText);
            },
            complete: function () {
                InitComposants();
            }
        });
    });

    // Onclick sur le bouton de validation du compte sélectionné
    $("body").on("click", "#btnConfirmUpdateCompte", function (e) {

        var url = $(this).data("url");

        var NumCompte = $("[name='rbComptes']:checked").val().split("|")[0];
        var LbCompte = $("[name='rbComptes']:checked").val().split("|")[1];

        $.ajax({
            type: "POST",
            url: url,
            data: {
                NumCompte: NumCompte,
                LbCompte: LbCompte
            },
            success: function (result) {
                if (result.Etat != null) {
                    if (result.Etat == "ok") {
                        AjouterFlash("alert-success", "Compte de remboursement modifié");
                        $("#txtCompte").html(NumCompte + " (" + LbCompte + ")");
                    }
                    else
                        AjouterFlash("alert-danger", "Erreur lors de la validation du véhicule");
                }
            },
            error: function (error) {
                alert(error.responseText);
            },
            complete: function () {
            }
        });
    });



    ///////////////////////////////////////////////////
    // Methodes pour la validation des cartes grises //
    ///////////////////////////////////////////////////
    $("body").on("click", "#btnOpenCarteGrise", function () {

        $("#modal .modal-dialog").width($(this).data("width"));
        var url = $(this).data("url");
        var contentType = $(this).data("contenttype");

        var base = "<div class=\"modal-body\">";

        base += "<button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>";

        base += "<div>";
        base += "<object id=\"fileView\"";

        if (contentType.indexOf("pdf") > -1)
            base += " height=\"600px\"";

        base += " width=\"100%\"";
        base += "type=\"" + contentType + "\" data=\"" + url + "\"></object>";
        base += "</div>";

        base += "</div>";

        $(".modal-content").html(base);
    });

    /////////////////////////////////////////////////
    // Initialisation de la datatable des demandes //
    /////////////////////////////////////////////////
    $('#tabDemandes').DataTable({
        "order": [[1, "desc"]],
        //"pageLength": 3,
        language: {
            processing: "Traitement en cours...",
            search: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ enregistrements",
            info: "Affichage des enregistrements _START_ &agrave; _END_ sur _TOTAL_",
            infoEmpty: "Affichage de l'enregistrement 0 &agrave; 0 sur 0 enregistrement",
            infoFiltered: "(filtr&eacute; de _MAX_ enregistrements au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun enregistrement &agrave; afficher",
            emptyTable: "Aucun enregistrement disponible",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        }
    });

    ///////////////////////////////////////////////////////////
    //// Initialisation de la datatable des demandes en Ajax //
    ///////////////////////////////////////////////////////////
    $('#tabDemandesAjax').DataTable({
        "order": [[1, "desc"]],
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": $('#tabDemandesAjax').data("url"),
        "aoColumns": [
            { "data": "Id" },
            { "data": "Alias" },
            { "data": "Mois" },
            { "data": "Etat" },
            { "data": "Montant" }
        ],
        language: {
            processing: "Traitement en cours...",
            search: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ enregistrements",
            info: "Affichage des enregistrements _START_ &agrave; _END_ sur _TOTAL_",
            infoEmpty: "Affichage de l'enregistrement 0 &agrave; 0 sur 0 enregistrement",
            infoFiltered: "(filtr&eacute; de _MAX_ enregistrements au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun enregistrement &agrave; afficher",
            emptyTable: "Aucun enregistrement disponible",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            },
            "sProcessing": "<i class=\"fa fa-5x fa-spinner fa-spin spinner-attente\"></i>"
        }
    });

    $('#tabDemandesAjax tbody').on('click', 'tr', function () {
        if ($(this).attr("id").indexOf("Indisponible_") == -1)
            window.document.location = $("#tabDemandesAjax").data("url-action") + "/" + $(this).attr("id");
        else
            alert("Vous n'êtes pas autorisé à afficher cette demande");
    });


    /////////////////////////////////////////////////
    // Initialisation de la datatable de gestion des PDO //
    /////////////////////////////////////////////////
    $('#tabGestionPDO').DataTable({
        "order": [[0, "asc"]],
        "paging": false,
        "columnDefs": [ { "targets": 7, orderable: false } ],
        language: {
            processing: "Traitement en cours...",
            search: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ enregistrements",
            info: "Affichage des enregistrements _START_ &agrave; _END_ sur _TOTAL_",
            infoEmpty: "Affichage de l'enregistrement 0 &agrave; 0 sur 0 enregistrement",
            infoFiltered: "(filtr&eacute; de _MAX_ enregistrements au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun enregistrement &agrave; afficher",
            emptyTable: "Aucun enregistrement disponible",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        }
    });

    /////////////////////////////////////////////////////////////////////////////
    // Affichage de la modal pour la modification ou la création d'un PDO //
    /////////////////////////////////////////////////////////////////////////////
    $(".btnDetailPDO").click(function () {
        alert();
        $("#modal .modal-dialog").width($(this).data("width"));

        var base = "<div class=\"modal-header\">";
        base += "<button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>";
        base += "</div>";
        base += "<div class=\"modal-body text-center\" style=\"font-size: 10em;\">";
        base += "<i class=\"fa fa-fw fa-spinner fa-spin\"></i>";
        base += "</div>";
        $(".modal-content").html(base);

        var url = $(this).attr("data-path");
        $.ajax({
            type: "GET",
            url: url,
            cache: false,
            success: function (result) {
                $(".modal-content").html(result);
            },
            error: function (error) {
                alert(error.responseText);
            },
            complete: function () {
                $("#formDetailPDO").validate({
                    errorPlacement: function (error, element) {
                        if (element.hasClass("form-select2")) {
                            error.appendTo(element.next(".select2"));
                        }
                        else if (element.hasClass("icheck")) {
                            error.insertAfter(element.parents(".form-group").children(".control-label")[0]);
                        }
                        else {
                            error.insertAfter(element);
                        }
                    }
                });

                InitComposants();

                $("form").on("invalid-form", function () {
                    RemoveSpinnerAttente();
                });
            }
        });
    });

    //////////////////////////////////////////////
    // Appels AJAX pour la suppression d'un PDO //
    //////////////////////////////////////////////
    $("body").on("click", ".btnDelPDO", function () {

        if (!confirm("Etes-vous sûr de vouloir supprimer ce PDO ?")) {
            RemoveSpinnerAttente();
            return;
        }

        var url = $(this).data("url");

        window.document.location = url;
    });

    //////////////////////////////////////////////////////////////////
    // Appel AJAX pour récuperer les POs correpsondant au matricule //
    //////////////////////////////////////////////////////////////////
    $("body").on("keyup", ".matriculeToPO", function () {
        if ($(this).val().length == 7) {
            var url = $(this).data("path");

            $.ajax({
                type: "POST",
                url: url,
                data: { matricule: $(this).val() },
                success: function (result) {
                    $(".POrewrite").html("");
                    for (var i = 0; i < result.length; i++) {
                        $(".POrewrite").append("<option value=\"" + result[i].value + "\">" + result[i].name + "</option>");
                    }
                }
            });
        }
    });

    //////////////////////////////////////////////////////////////////
    // Methode pour l'écran de gestion des barèmes de remboursement //
    //////////////////////////////////////////////////////////////////
    $("#ddlDtApplication").on("change", function () {
        var url = $(this).data("url");
        var date = $(this).val();
        window.document.location = url + "?dtApplication=" + date.substring(3,5) + "-" + date.substring(0,2) + "-" + date.substring(6,10);
    });

    $("#btnAddBaremeRemboursement").on("click", function () {
        $("#modal .modal-dialog").width($(this).data("width"));

        var base = "<div class=\"modal-header\">";
        base += "<button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>";
        base += "<h4 class=\"modal-title\">Sélectionnez une date d'application</h4>";
        base += "</div>";

        base += "<div class=\"modal-body\">";
        base += "<div class=\"row\">";
        base += "	<div class=\"col-xs-12\">";
        base += "	    <div id=\"dpSelectDtApplication\" class=\"text-center\" data-rel=\"datepicker\"></div>";
        base += "	    <input type=\"hidden\" id=\"txtSelectDate\">";
        base += "	</div>";
        base += "	<div class=\"col-xs-12 text-center\">";
        base += "		<button id=\"btnConfirmSelectDtApplication\" class=\"btn btn-primary attente\" type=\"button\" data-url=\"" + $(this).data("url") + "\">Ok</button>";
        base += "		<button class=\"btn btn-default\" type=\"button\" data-dismiss=\"modal\">Annuler</button>";
        base += "	</div>";
        base += "</div>";
        base += "</div>";

        $(".modal-content").html(base);

        $("#formModifMontant").validate({
            invalidHandler: function (event, validator) {
                RemoveSpinnerAttente();
            }
        });

        $('#dpSelectDtApplication').datepicker({
            startView: 1,
            minViewMode: 0,
            language: "fr"
        });
        $('#dpSelectDtApplication').on("changeDate", function () {
            $('#txtSelectDate').val(
                $('#dpSelectDtApplication').datepicker('getFormattedDate')
            );
        });
        $(".datepicker-inline").css("display", "inline-block");

        AddRules();
    });
    $(".btnBaremeRemboursement").on("click", function () {
        var url = $(this).data("url");
        window.document.location = url;
    });
    $("#btnValiderModifBaremeRemboursement").on("click", function () {

        var ListBaremes = new Array();
        $(".montantBareme").each(
                function () {
                    var Id = $(this).data("idbareme");
                    var Valeur = $(this).val();
                    ListBaremes.push(Id + "|" + Valeur);
                }
        )

        var url = $(this).data("url");

        $.ajax({
            type: "GET",
            url: url,
            data: { ListBaremes: ListBaremes },
            traditional: true, 
            cache: false,
            success: function (result) {
                if (result.Etat != null) {
                    if (result.Etat == "ko") {
                        AjouterFlash("alert-danger", "Erreur lors de la modification des barèmes");
                    }
                    else if (result.Etat == "Redirect") {
                        window.location = result.Url;
                    }
                }
            },
            error: function (error) {
                alert(error.responseText);
            },
            complete: function () {
            }
        });
    });
    $("body").on("click", "#btnConfirmSelectDtApplication", function () {

        var DateSelectionnee = $("#txtSelectDate").val();

        if (DateSelectionnee == "") {
            alert("Veuillez sélectionner la date d'application du barème !")
            RemoveSpinnerAttente();
            return;
        }

        var url = $(this).data("url");

        url += "?dtApplication=" + DateSelectionnee.substring(3, 5) + "-" + DateSelectionnee.substring(0, 2) + "-" + DateSelectionnee.substring(6, 10);

        window.document.location = url;
    });


});